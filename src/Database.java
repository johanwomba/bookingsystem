import java.sql.*;


public class Database extends Admin {

	public Database() {
		
	}
	public static String DeleteDatabase(){
		try {
			Connection myConn = DriverManager.getConnection(URL, username, pass);
			Statement myStmt = myConn.createStatement();
			myStmt.executeUpdate("DROP SCHEMA IF EXISTS flight_system;");
			myStmt.executeUpdate("CREATE SCHEMA flight_system;");
			myStmt.executeUpdate("CREATE TABLE flight_system.airports_departure (airport_id INT NOT NULL AUTO_INCREMENT,"
									+ "airport_name VARCHAR(45) NULL, airport_town VARCHAR(45) NULL,"
									+ "airport_country VARCHAR(45) NULL, PRIMARY KEY (airport_id),"
									+ " UNIQUE INDEX airport_name_UNIQUE (airport_name ASC));");
			myStmt.executeUpdate("CREATE TABLE flight_system.airports_destination (airport_id INT NOT NULL AUTO_INCREMENT,"
									+ "airport_name VARCHAR(45) NULL, airport_town VARCHAR(45) NULL,"
									+ "airport_country VARCHAR(45) NULL, PRIMARY KEY (airport_id),"
									+ " UNIQUE INDEX airport_name_UNIQUE (airport_name ASC));");
			myStmt.executeUpdate("CREATE TABLE flight_system.airlines (airline_id INT NOT NULL AUTO_INCREMENT,"
									+ "airline_name VARCHAR(45) NOT NULL, airline_country VARCHAR (45) NULL, "
									+ "PRIMARY KEY (airline_id, airline_name), UNIQUE INDEX airline_name_UNIQUE (airline_name ASC));");
			myStmt.executeUpdate("CREATE TABLE flight_system.flights (flight_id INT NOT NULL AUTO_INCREMENT,"
									+ " flight_name VARCHAR(5) NULL, flight_departure INT(11) NOT NULL,"
									+ " flight_destination INT(11) NOT NULL, flight_airline INT(11) NULL, "
									+ "departure_date VARCHAR(45) NULL, PRIMARY KEY (flight_id, flight_departure, flight_destination),"
									+ " UNIQUE INDEX flight_name_UNIQUE (flight_name ASC), UNIQUE INDEX departure_date_UNIQUE"
									+ " (departure_date ASC), INDEX FK_flights_departure_idx (flight_departure ASC),"
									+ " INDEX FK_flights_destination_idx (flight_destination ASC),"
									+ " INDEX FK_fligts_airline_idx (flight_airline ASC),"
									+ " CONSTRAINT FK_flights_departure FOREIGN KEY (flight_departure)"
									+ " REFERENCES flight_system.airports_departure (airport_id)"
									+ " ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT FK_flights_destination"
									+ " FOREIGN KEY (flight_destination) REFERENCES flight_system.airports_destination"
									+ " (airport_id) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT FK_fligts_airline"
									+ " FOREIGN KEY (flight_airline) REFERENCES flight_system.airlines (airline_id)"
									+ " ON DELETE NO ACTION ON UPDATE NO ACTION);");
			myStmt.executeUpdate("CREATE TABLE flight_system.bookings (booking_id INT NOT NULL AUTO_INCREMENT,"
									+ " booking_firstname VARCHAR(45) NULL, booking_lastname VARCHAR (45) NULL,"
									+ " booking_price VARCHAR (45) NULL, booking_flight INT(11) NOT NULL, PRIMARY KEY (booking_id, booking_flight),"
									+ " CONSTRAINT FK_booking_flight FOREIGN KEY (booking_flight) REFERENCES flight_system.flights"
									+ " (flight_id) ON DELETE NO ACTION ON UPDATE NO ACTION);");
			myStmt.executeUpdate("CREATE TABLE flight_system.customer (customer_id INT NOT NULL AUTO_INCREMENT,"
									+ " customer_firstname VARCHAR(45) NULL, customer_lastname VARCHAR(45) NULL,"
									+ " customer_phonenr VARCHAR(45) NULL, customer_email VARCHAR(45) NULL, PRIMARY KEY"
									+ " (customer_id));");
			}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return "Database deleted.";
	}
	public static String ExampleData(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			/*---------------------------------------------Airports--------------------------------------------------*/
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('1', 'ARN', 'Stockholm', 'Sweden');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('2', 'OSD', 'Ístersund', 'Sweden');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('3', 'AMS', 'Amsterdam', 'Netherlands');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('4', 'LAX', 'Los Angeles', 'USA');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('5', 'BKK', 'Bangkok', 'Thailand');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('6', 'AUC', 'Auckland', 'New Zealand');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('7', 'LHR', 'London', 'Great Britain');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('8', 'BER', 'Berlin', 'Germany');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('9', 'SYD', 'Sydney', 'Australia');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('10', 'RKV', 'Reykjavik', 'Iceland');");
			/*---------------------------------------------Airports--------------------------------------------------*/
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('1', 'ARN', 'Stockholm', 'Sweden');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('2', 'OSD', 'Ístersund', 'Sweden');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('3', 'AMS', 'Amsterdam', 'Netherlands');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('4', 'LAX', 'Los Angeles', 'USA');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('5', 'BKK', 'Bangkok', 'Thailand');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('6', 'AUC', 'Auckland', 'New Zealand');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('7', 'LHR', 'London', 'Great Britain');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('8', 'BER', 'Berlin', 'Germany');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('9', 'SYD', 'Sydney', 'Australia');");
			myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_id, airport_name, airport_town,"
					+ " airport_country) VALUES ('10', 'RKV', 'Reykjavik', 'Iceland');");
			/*---------------------------------------------Airlines--------------------------------------------------*/
			myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('1', 'JohanAIR', 'Sweden');");
			myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('2', 'ScandinavianAIR', 'Sweden');");
			myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('3', 'KillerAirWay', 'Great Britain');");
			myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('4', 'TommeTomat', 'Denmark');");
			myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_id, airline_name, airline_country) VALUES"
					+ " ('5', 'RyanAir', 'Germany');");
			/*-----------------------------------------------Flights-------------------------------------------------------*/
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('1', 'SK571', '1', '5', '1', '2015-06-01');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('2', 'SK819', '2', '8', '2', '2015-05-01');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('3', 'AR912', '3', '1', '3', '2015-05-10');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('4', 'GS846', '9', '10', '5', '2015-12-24');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('5', 'LU782', '6', '4', '4', '2015-09-15');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('6', 'SK123', '8', '7', '3', '2015-12-01');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('7', 'SK574', '5', '4', '2', '2015-01-15');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('8', 'SK901', '9', '3', '3', '2015-05-03');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('9', 'GB666', '3', '2', '1', '2015-11-03');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('10', 'KP001', '10', '1', '5', '2015-11-01');");
			myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_id, flight_name, flight_departure, flight_destination,"
					+ " flight_airline, departure_date) VALUES ('11', 'SD101', '10', '1', '5', '2015-11-02');");
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return "Database with example data complete.";
	}
}