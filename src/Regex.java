import java.util.regex.Pattern;


public class Regex {
	
	public static boolean RegexAirport (String validate){
		Pattern pattern = Pattern.compile("^[a-zA-Z]{3}$");
		return pattern.matcher(validate).matches();
	}
	public static boolean RegexAirline (String validate){
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{6,}$");
		return pattern.matcher(validate).matches();
	}
	public static boolean RegexFlight (String validate){
		Pattern pattern = Pattern.compile("^[a-zA-Z]{2}[0-9]{3}$");
		return pattern.matcher(validate).matches();
	}
	public static boolean RegexDate (String validate){
		Pattern pattern = Pattern.compile("^[0-9]{4}-[0-9]{2}-[0-9]{2}$");
		return pattern.matcher(validate).matches();
	}
	public static boolean RegexName (String validate){
		Pattern pattern = Pattern.compile("^[a-�A-�]{3,12}$");
		return pattern.matcher(validate).matches();
	}
	public static boolean RegexPhone(String validate){
		Pattern pattern = Pattern.compile("^[0-9]{6,10}$");
		return pattern.matcher(validate).matches();
	}
	public static boolean RegexEmail(String validate){
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9\\.]+@[a-zA-Z0-9\\.]+\\.[a-zA-Z]{2,4}$");
		return pattern.matcher(validate).matches();
	}
}
