import java.sql.*;

public class Admin {

	static String URL = "jdbc:mysql://localhost:3306/ya";
	static String username = "root";
	static String pass = "root";
	
	public Admin() {

	}
	public static String CreateAirport(String airport_name, String airport_town, String airport_country){
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.airports_departure (airport_name, airport_town, airport_country)"
						+ " VALUES('" + airport_name + "', '" + airport_town + "', '" + airport_country +"');");
				myStmt.executeUpdate("INSERT INTO flight_system.airports_destination (airport_name, airport_town, airport_country)"
						+ " VALUES('" + airport_name + "', '" + airport_town + "', '" + airport_country +"');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		return "Airport named " + airport_name + " created.";
	}
	public static String CreateAirline(String airline_name, String airline_country){ 
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.airlines (airline_name, airline_country) VALUES('" 
													+ airline_name + "', '" + airline_country + "');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		return "Airline named " + airline_name + " created.";
	}
	public static String CreateFlight(String flight_name, int departure, int destination, int airline, String departureDate){ 
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.flights (flight_name, flight_departure, flight_destination,"
											+ " flight_airline, departure_date) VALUES('" + flight_name + "', '"
											+ departure + "', '" + destination + "', '" + airline + "', '" + departureDate +"');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
		return "Flight named " + flight_name + " created.";
	}
	public static void CreateBooking (String firstName, String lastName, int price, String chooseFlight){
			try {
				Connection myConn = DriverManager.getConnection(URL, username, pass);
				Statement myStmt = myConn.createStatement();
				myStmt.executeUpdate("INSERT INTO flight_system.bookings (booking_firstname, booking_lastname, booking_price,"
						+ " booking_flight) VALUES('" + firstName + "', '" + lastName + "', '" + price + "', '" + chooseFlight + "');");
			}
			catch (Exception exc){
				exc.printStackTrace();
			}
	}
}