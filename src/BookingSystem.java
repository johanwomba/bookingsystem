import java.util.Random;
import java.util.Scanner;

public class BookingSystem {

	public static void main(String[] args) {
		Scanner user_input = new Scanner(System.in);
		Random randNr = new Random();
		int firstChoice;
		String firstName = null, lastName = null, phoneNr = null, eMail = null;
		Customer user = new Customer (firstName, lastName, phoneNr, eMail);
		System.out.println("Welcome! Would you like to login as (1) customer, or (2) as admin?");
		while (!user_input.hasNextInt()){
			System.out.println("Only numbers please.");
			user_input.next();
		}
		mainLoop: while (true){
			System.out.println("Before you can use this program, we need some credentials;");
			firstChoice = user_input.nextInt();
			System.out.println("Please enter your first name: ");
			firstName = user_input.next();
				while (!Regex.RegexName(firstName)){
					System.out.println("Not a valid name, only letters (at least three.)" + "\n" + "Please try again.");
					firstName = user_input.next();
				}
			System.out.println("Please enter your last name: ");
			lastName = user_input.next();
				while (!Regex.RegexName(lastName)){
					System.out.println("Not a valid name, only letters (at least three.)" + "\n" + "Please try again.");
					lastName = user_input.next();
				}
		if (firstChoice == 1){									//Customer part of program.
				System.out.println("Please enter your phone number: ");
				phoneNr = user_input.next();
					while (!Regex.RegexPhone(phoneNr)){
						System.out.println("Not a valid phone number. (6-10 numbers only)" + "\n" + "Please try again.");
						phoneNr = user_input.next();
					}
				System.out.println("Please enter your e-mail: ");
				eMail = user_input.next();
					while (!Regex.RegexEmail(eMail)){
						System.out.println("Not a valid e-mail. (Example: test@test.com)" + "\n" + "Please try again.");
						eMail = user_input.next();
					}
			Customer.CreateCustomer(firstName, lastName, phoneNr, eMail);
			System.out.println("Welcome " + firstName + " " + lastName + "!");
			while (true){
			customerLoop: while (true){
			int customerChoice;
			System.out.println("What would you like to do?");
			System.out.println("(1) List all available flights" + "\n" + "(2) Book flight" + "\n" + "(3) Print ticket"
								+ "\n" + "(4) View credentials." + "\n" + "(5) Exit.");
			customerChoice = user_input.nextInt();
			if (customerChoice == 1){
				Listing.ListFlights();
			}
			if (customerChoice == 2){							//Book flight
				int price;
				price = randNr.nextInt(15000) + 1000;
				String flyFrom, flyTo, bookFlight, bookSure, chooseFlight;
				Listing.ListAirports();
				System.out.println("Where would you like to fly from? (Enter airport-ID) ");
				flyFrom = user_input.next();
				System.out.println("Where are you going? (Enter airport-ID) ");
				flyTo = user_input.next();
				if (flyFrom.equals(flyTo)){
					System.out.println("You can obviously not choose this option. Returning to main menu.");
					break customerLoop;
				}
				else {
				System.out.println("Result: ");
				boolean checkFlight = Listing.SearchFlight(flyFrom, flyTo);
				if (checkFlight){
					System.out.println("These are the available flights flying the entered route.");
					System.out.println("Would you like to book flight? (yes/no)");
					bookFlight = user_input.next();
					if(bookFlight.equals("yes")){
						System.out.println("Enter ID of the flight you would like to book: ");
						chooseFlight = user_input.next();
						System.out.println("The price for this flight is " + price + " kr. Are you sure?");
						bookSure = user_input.next();
							if (bookSure.equals("yes")){
								Admin.CreateBooking(firstName, lastName, price, chooseFlight);
								System.out.println("Success! Flight booked.");
							}
							else if (bookSure.equals("no")) {
								System.out.println("Flight not booked. Returning to main menu.");
								break customerLoop;
							}
					else if (bookFlight.equals("no")){
						System.out.println("Flight not booked. Returning to main menu.");
						break customerLoop;
					}
				}
				}
			else {
				System.out.println("No flights flying that route. Returning to main menu.");
				break customerLoop;
			}
		}
	}
			else if (customerChoice == 3){						//Print ticket. Checks the entered name from
				Listing.PrintTicket(firstName, lastName);		//beginning of program, then prints result.
			}
			else if (customerChoice == 4){						//Exit program
				System.out.println(user.getCustomer(firstName, lastName, phoneNr, eMail) + "\n");
			}
			else if (customerChoice == 5){						//Exit program
				System.out.println("Good bye.");
				break mainLoop;
			}
		}
	}
}
		else if (firstChoice == 2){								//Admin part of program.
			String delDatabase, exampleData;
			System.out.println("Welcome " + firstName + " " + lastName + "!");
			System.out.println("Would you like to renew the current database? yes/no");
			delDatabase = user_input.next();
			if (delDatabase.equals("yes")){
				System.out.println(Database.DeleteDatabase());
				System.out.println("Would you like some example data? yes/no");
				exampleData = user_input.next();
				if (exampleData.equals("yes")){
					System.out.println(Database.ExampleData());
				}
				else if (exampleData.equals("no")){
					System.out.println("Database will be empty.");
				}
			}
			else if (delDatabase.equals("no")){
				System.out.println("Old database will be used.");
			}
			while(true){										//Two loops for foolproofing.
			adminLoop: while(true){
			int adminChoice;
			System.out.println("What would you like to do?");
			System.out.println("(1) Create new airport" + "\n" + "(2) Create airline" + "\n" + "(3) Create flight" + 
							   "\n" + "(4) List total bookings" + "\n" + "(5) List total airports" + "\n" +
							   "(6) List total airlines" + "\n" + "(7) List total flights" + "\n" + "(8) List registered customers."
							   + "\n" + "(9) Exit.");
			adminChoice = user_input.nextInt();
			if (adminChoice == 1){								//Create airport
				String airport_name, airport_town, airport_country;
				System.out.println("Enter name of airport(3 letters (a-z), nothing more, nothing less): ");
				airport_name = user_input.next();
				if (!Regex.RegexAirport(airport_name)){
					System.out.println("Invalid input! Only exact 3 letters (a-z) allowed. returning to main menu.");
					break adminLoop;
				}
				System.out.println("Enter town of airport (1 word): ");
				airport_town = user_input.next();
				System.out.println("Enter country of airport (1 word): ");
				airport_country = user_input.next();
				System.out.println(Admin.CreateAirport(airport_name, airport_town, airport_country));
			}
			else if (adminChoice == 2){							//Create airline
				String airline_name, airline_country;
				System.out.println("Enter name of airline (At least 6 characters): ");
				airline_name = user_input.next();
				if (!Regex.RegexAirline(airline_name)){
					System.out.println("Invalid entry! You need at least 6 characters. Returning to main menu.");
					break adminLoop;
				}
				System.out.println("Enter country where this airline is based: ");
				airline_country = user_input.next();
				System.out.println(Admin.CreateAirline(airline_name, airline_country));
			}
			else if (adminChoice == 3){							//Create flight
				String flight_name, departureDate;
				int departure, destination, airline;
				Listing.ListAirports();
				System.out.println("Enter departure airport-ID from above: ");
				departure = user_input.nextInt();
				System.out.println("Enter destination airport-ID from above: ");
				destination = user_input.nextInt();
				if (destination == departure){
					System.out.println("You can obviously not choose the same airport. Returning to main menu.");
					break adminLoop;
				}
				else {
				Listing.ListAirlines();
				System.out.println("Enter airline-ID from above: ");
				airline = user_input.nextInt();
				System.out.println("Enter date (as 20xx-xx-xx): ");
				departureDate = user_input.next();
				if (!Regex.RegexDate(departureDate)){
					System.out.println("Invalid entry! Must be as 20xx-xx-xx. Returning to main menu.");
					break adminLoop;
				}
				System.out.println("Enter name of flight (Two letters, three numbers. (xx123): ");
				flight_name = user_input.next();
				if (!Regex.RegexFlight(flight_name)){
					System.out.println("Invalid entry! You need two letters, three numbers. Returning to main menu.");
					break adminLoop;
				}
				System.out.println(Admin.CreateFlight(flight_name, departure, destination, airline, departureDate));
				}
			}
			else if (adminChoice == 4){								//List bookings
				Listing.ListBookings();
			}
			else if (adminChoice == 5){								//List airports
				Listing.ListAirports();
			}
			else if (adminChoice == 6){								//List airlines
				Listing.ListAirlines();
			}
			else if (adminChoice == 7){								//List flights
				Listing.ListFlights();
			}
			else if (adminChoice == 8){								//List customers whom have entered credentials in program.
				Listing.ListCustomers();
			}
			else if (adminChoice == 9){								//Exit program
				System.out.println("Good bye.");
				break mainLoop;
			}
		}
	}
	}
	else {
			System.out.println("Only 1 or 2, cant you read?");
		}
}
		user_input.close();
	}
}



/*
 * Author:
 * 
 * Johan Olsson
 * 2015-01-10
 * 
 * */