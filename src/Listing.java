import java.sql.*;

public class Listing {

	public Listing() {
		
}
	public static void ListAirports(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.airports_departure");
			while (result.next()){
			System.out.println("("+ result.getString("airport_id") + ") "+ result.getString("airport_town")
					+ " (" + result.getString("airport_name") + ")");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void ListAirlines(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.airlines");
			while (result.next()){
			System.out.println("(" + result.getString("airline_id") + ") " + result.getString("airline_name") + "\n" +"Based in: "
										+ result.getString("airline_country") + "\n");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void ListFlights(){
		System.out.println("_______________________________________________________________________");
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.flights LEFT JOIN flight_system.airports_departure"
						+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination"
						+ " ON flight_destination = flight_system.airports_destination.airport_id"
						+ " LEFT JOIN flight_system.airlines ON flight_airline = airline_id");
			while (result.next()){
			System.out.println("(" + result.getString("flight_id") + ") " + result.getString("flight_name") + " Departing from: "
						+ result.getString("airport_town") + " (" + result.getString("airport_name") + ") - "
					    + "Destination: " + result.getString("airports_destination.airport_town")
						+ " (" + result.getString("airports_destination.airport_name") + ")" + "\n" + "\t" + " Flying with "
					    + result.getString("airline_name") + ". Flight Date: " + result.getString("departure_date"));
			System.out.println("_______________________________________________________________________");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void ListBookings(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.bookings LEFT JOIN flight_system.flights"
						+ " ON flight_id = booking_flight LEFT JOIN flight_system.airlines ON"
						+ " flight_airline = airline_id LEFT JOIN flight_system.airports_departure"
						+ " ON flight_departure = airport_id LEFT JOIN flight_system.airports_destination"
						+ " ON flight_destination = flight_system.airports_destination.airport_id");
			while (result.next()){
			System.out.println("||Flight: " + result.getString("flights.flight_name") + " || Passenger: "
						+ result.getString("booking_firstname")	+ " " + result.getString("booking_lastname")
						+ " || Price: " + result.getString("booking_price") + " SEK" + " || Flying from: "
						+ result.getString("airports_departure.airport_town") + " || To: "
						+ result.getString("airports_destination.airport_town") + " || Flying with: " 
						+ result.getString("airlines.airline_name") + " || Date: " + result.getString("flights.departure_date") + " ||");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static void ListCustomers(){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.customer");
			while (result.next()){
			System.out.println("|| " + result.getString("customer_firstname") + " || "+ result.getString("customer_lastname")
								+ " || Phone number: " + result.getString("customer_phonenr")
								+ " || E-mail: " + result.getString("customer_email") + " ||");
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
	public static boolean SearchFlight(String flyFrom, String flyTo){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.flights WHERE flight_departure"
													+ " = '" + flyFrom + "' AND flight_destination = '" + flyTo
													+ "';");
			while (result.next()){
				System.out.println("ID: " + result.getString("flight_id") + " Flight name: " + result.getString("flight_name")
						+ " Flight date: " + result.getString("departure_date"));
				return true;
			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
		return false;
	}
	public static void PrintTicket(String firstName, String lastName){
		try{
			Connection myConn = DriverManager.getConnection(Admin.URL, Admin.username, Admin.pass);
			Statement myStmt = myConn.createStatement();
			ResultSet result = myStmt.executeQuery("select * from flight_system.bookings LEFT JOIN flight_system.flights"
								+ " ON flight_id = booking_flight LEFT JOIN flight_system.airlines ON"
								+ " flight_airline = airline_id RIGHT JOIN flight_system.airports_departure"
								+ " ON flight_departure = flight_system.airports_departure.airport_id RIGHT JOIN flight_system.airports_destination"
								+ " ON flight_destination = flight_system.airports_destination.airport_id WHERE booking_firstname"
								+ " = '" + firstName + "' AND booking_lastname = '" + lastName + "';");
			
			while (result.next()){
			System.out.println("*********************************************");
			System.out.println("*Date: "+ "\t" + "\t" + result.getString("flights.departure_date") + "\n" + "*Flight: "
								+ "\t" + result.getString("flights.flight_name") + "\n" + "*Passenger: " + "\t" 
								+ result.getString("booking_firstname") + " " + result.getString("booking_lastname") + "\n"
								+ "*Paying: " + "\t" + result.getString("booking_price") + " SEK" + "\n" + "*Departure: " + "\t"
								+ result.getString("airports_departure.airport_town") +
								" (" + result.getString("airports_departure.airport_name") + ")" + "\n" + "*Destination: " + "\t"
								+ result.getString("airports_destination.airport_town") +
								" (" + result.getString("airports_destination.airport_name") + ")" + "\n" + "*" + "\n"
								+ "*Have a nice flight!" + "\n" + "*Thank you for flying with " + result.getString("airline_name"));
			System.out.println("*********************************************");

			}
		}
		catch (Exception exc){
			exc.printStackTrace();
		}
	}
}